/* eslint-disable react/display-name */
import {
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator,
  DrawerItems,
  SafeAreaView,
} from 'react-navigation';
import { Expo } from 'expo';
import { Icon } from 'react-native-elements';
import { Image, Platform, ScrollView, StyleSheet, Text, View } from 'react-native';
import React, { Component } from 'react';

import { DISHES } from '../shared/dishes';
import AboutUs from './AboutUsComponent';
import ContactUs from './ContactUs';
import DishDetail from './DishdetailComponent';
import Home from './HomeComponent';
import Menu from './MenuComponent';

const MenuNavigator = createStackNavigator(
  {
    Menu: {
      screen: Menu,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (
          <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        ),
      }),
    },
    Dishdetail: { screen: DishDetail },
  },
  {
    initialRouteName: 'Menu',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#512DA8',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff',
      },
    },
  }
);

const AboutUsNavigator = createStackNavigator(
  {
    AboutUs: {
      screen: AboutUs,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (
          <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        ),
      }),
    },
  },
  {
    initialRouteName: 'AboutUs',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#512DA8',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff',
      },
    },
  }
);

const ContactUsNavigator = createStackNavigator(
  {
    ContactUs: { screen: ContactUs },
  },
  {
    initialRouteName: 'ContactUs',
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff',
      },
      headerLeft: (
        <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
      ),
    }),
  }
);

const HomeNavigator = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (
          <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        ),
      }),
    },
    Dishdetail: { screen: DishDetail },
  },
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff',
      },
      headerLeft: (
        <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
      ),
    }),
  }
);

const CustomDrawerContentComponent = props => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.drawerHeader}>
        <View style={{ flex: 1 }}>
          <Image source={require('./images/logo.png')} style={styles.drawerImage} />
        </View>
        <View style={{ flex: 2 }}>
          <Text style={styles.drawerHeaderText}>My First React App</Text>
        </View>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

const MainNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeNavigator,
      navigationOptions: {
        title: 'Home',
        drawerLabel: 'Home',
        drawerIcon: ({ tintColor, focused }) => (
          <Icon name="home" type="font-awesome" size={24} color={tintColor} />
        ),
      },
    },
    ContactUs: {
      screen: ContactUsNavigator,
      navigationOptions: {
        title: 'Contact us',
        drawerLabel: 'Contact us',
        drawerIcon: ({ tintColor }) => (
          <Icon name="address-card" type="font-awesome" size={22} color={tintColor} />
        ),
      },
    },
    Menu: {
      screen: MenuNavigator,
      navigationOptions: {
        title: 'Menu',
        drawerLabel: 'Menu',
        // eslint-disable-next-line react/display-name
        drawerIcon: ({ tintColor }) => (
          <Icon name="list" type="font-awesome" size={24} color={tintColor} />
        ),
      },
    },
    AboutUs: {
      screen: AboutUsNavigator,
      navigationOptions: {
        title: 'About us',
        drawerLabel: 'AboutUs',
        drawerIcon: ({ tintColor }) => (
          <Icon name="info-circle" type="font-awesome" size={24} color={tintColor} />
        ),
      },
    },
  },
  {
    drawerBackgroundColor: '#D1C4E9',
    contentComponent: CustomDrawerContentComponent,
  }
);

const MainNav = createAppContainer(MainNavigator);

export class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dishes: DISHES,
      selectedDish: null,
    };
  }

  onDishSelect(dishId) {
    this.setState({ selectedDish: dishId });
  }

  render() {
    return (
      <View
        style={{ flex: 1, paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight }}
      >
        <MainNav />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerHeader: {
    backgroundColor: '#512DA8',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
  },
  drawerHeaderText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  drawerImage: {
    margin: 10,
    width: 80,
    height: 60,
  },
});

export default Main;
