import { Card, ListItem } from 'react-native-elements';
import { Image, ScrollView, Text, View } from 'react-native';
import React, { Component } from 'react';

import { LEADERS } from '../shared/leaders';

function RenderLeader(props) {
  const leaders = props.leader;

  const ListLeaders = leaders.map((l, i) => (
    <ListItem
      key={i}
      title={l.name}
      subtitle={l.description}
      hideChevron={true}
      avatar={<Image source={require('./images/alberto.png')} />}
      avatarStyle={{ borderRadius: 50, height: 50, width: 50 }}
    />
  ));

  return <Card title="Corporate Leadership">{ListLeaders}</Card>;
}

class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leaders: LEADERS,
    };
  }

  static navigationOptions = () => {
    return {
      title: 'Home',
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#512DA8',
      },
    };
  };

  render() {
    return (
      <View>
        <ScrollView>
          <Card title="Our History">
            <Text>
              Started in 2010, Ristorante con Fusion quickly established itself as a culinary icon
              par excellence in Hong Kong. With its unique brand of world fusion cuisine that can be
              found nowhere else, it enjoys patronage from the A-list clientele in Hong Kong.
              Featuring four of the best three-star Michelin chefs in the world, you never know what
              will arrive on your plate the next time you visit us. The restaurant traces its humble
              beginnings to The Frying Pan, a successful chain started by our CEO, Mr. Peter Pan, //
              eslint-disable-next-line react/no-unescaped-entities that featured for the first time
              the world`s best cuisines in a pan.
            </Text>
          </Card>

          <RenderLeader leader={this.state.leaders} />
        </ScrollView>
      </View>
    );
  }
}

export default AboutUs;
