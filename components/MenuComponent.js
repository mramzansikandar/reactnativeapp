import { FlatList } from 'react-native';
import { ListItem } from 'react-native-elements';
import React, { Component } from 'react';

import { DISHES } from '../shared/dishes';

class Menu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dishes: DISHES,
    };
  }

  static navigationOptions = () => {
    return {
      title: 'Home',
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#512DA8',
      },
    };
  };

  render() {
    const renderMenuItem = ({ item, index }) => {
      return (
        <ListItem
          key={index}
          title={item.name}
          subtitle={item.description}
          hideChevron={true}
          onPress={() => navigate('Dishdetail', { dishId: item.id })}
          roundAvatar
          avatar={{ uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg' }}
          //avatar ={{ source: require('./images/uthappizza.png')}}
          //avatar={<Image source={require('./images/uthappizza.png')}/>}
        />
      );
    };

    const { navigate } = this.props.navigation;

    return (
      <FlatList
        data={this.state.dishes}
        renderItem={renderMenuItem}
        keyExtractor={item => item.id.toString()}
      />
    );
  }
}

export default Menu;
