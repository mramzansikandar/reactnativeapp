import { Card } from 'react-native-elements';
import { Text } from 'react-native';
import React, { Component } from 'react';

class ContactUs extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Card title="Contact Information">
        <Text>
          121, Clear Water Bay Road Clear Water Bay, Kowloon HONG KONG Tel: +852 1234 5678 Fax: +852
          8765 4321 Email:confusion@food.net
        </Text>
      </Card>
    );
  }
}

export default ContactUs;
