import React from 'react';
import Main from "./components/MainComponents";
import { SafeAreaView, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <Main />
      </SafeAreaView>
    );
  }
}
