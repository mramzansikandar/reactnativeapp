import { applyMiddleware, combineReducers, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import { comments } from './comments';
import { dishes } from './dishes';
import { leaders } from './leaders';
import { promotions } from './promotions';

export const ConfigureStore = () => {
  const store = createStore(
    combineReducers({
      dishes,
      comments,
      promotions,
      leaders,
    }),
    applyMiddleware(thunk, logger)
  );

  return store;
};
